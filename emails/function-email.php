<?php

// applied html email
function dn_apply_email_content( $content, $email_heading = null ){

    // wrap all the content with header and footer
    ob_start();
    include(  get_template_directory() . '/emails/template/email-header.php');
    echo $content;
    include(  get_template_directory() . '/emails/template/email-footer.php') ;
    $_content = ob_get_clean();


    ob_start();
    include(  get_template_directory() . '/emails/template/email-styles.php');
    $css = apply_filters( 'woocommerce_email_styles', ob_get_clean() );
    
    // Grab new emogrifier function from woocommerce
    if ( class_exists( 'DOMDocument' ) && version_compare( PHP_VERSION, '5.5', '>=' ) ) {
        $emogrifier_class = '\\Pelago\\Emogrifier';
        if ( ! class_exists( $emogrifier_class ) ) {
            include_once get_template_directory() . '/emails/class-emogrifier.php';
        }
        try {
            $emogrifier = new $emogrifier_class( $_content, $css );
            $_content    = $emogrifier->emogrify();
        } catch ( Exception $e ) {
            $logger = wc_get_logger();
            $logger->error( $e->getMessage(), array( 'source' => 'emogrifier' ) );
        }
    } else {
        $_content = '<style type="text/css">' . $css . '</style>' . $_content;
    }
    
    return $_content;

}

// ADD HEADER AND FOOTER FOR ALL OUTGOING EMAIL
add_filter( 'wp_mail', 'dn_wp_mail_header_footer', 10, 1 );
function dn_wp_mail_header_footer( $args ) {

    $original_message = $args['message'];

    if (strpos($original_message, 'dont-apply-theme-style') !== false) {
        $args['message'] = $original_message;
    }else{
        $args['message'] = dn_apply_email_content($original_message);
    }
	
	return $args;
}

// CONTENT TYPE
add_filter( 'wp_mail_content_type', 'dn_wp_mail_set_content_type' );
function dn_wp_mail_set_content_type( $content_type ) {
	return 'text/html';
}
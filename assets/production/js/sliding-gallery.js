jQuery(function($) {
	// Hero image repeater container
	if ($(".dn-image-hero-repeats").length > 0) {
  
	  // Create lightboxes
	  $('.dn-image-hero-repeats .image-container').magnificPopup({
		type: 'image',
		gallery: {
		  enabled: true
		},
		callbacks: {
		  elementParse: function(item) {
			// the class name
  
			if ($(item.el.context).hasClass('video-link')) {
			  item.type = 'iframe';
			} else {
			  item.type = 'image';
			}
		  }
		},
	  });
  
	  // Don't run the slider if
	  if ($(window).width() >= 768) { // Desktop
		// If there's less than 8 images
		if ($(".dn-image-hero-repeats").children().length < 5) {
		  return;
		}
	  } else { // Mobile
		// If there's only 1 image
		if ($(".dn-image-hero-repeats").children().length < 2) {
		  return;
		}
	  }
  
	  // Determine the type of slider it will be
	  var ImageHeroCount = $(".dn-image-hero-repeats").children().length;
	  var HeroImagerows = 2;
	  // If there are very few images or we are on mobile go for a single row
	  if (ImageHeroCount < 8 || $(window).width() < 768) {
		HeroImagerows = 1;
	  }
  
	  // Remove the default flex container
	  $(".dn-image-hero-repeats").removeClass("flex-container");
  
	  if (HeroImagerows > 1) {
  
		// Setup the slider
		$('.dn-image-hero-repeats').slick({
		  "rows": 2,
		  "slidesPerRow": 4,
		  "slidesToScroll": 1 / 4,
		  infinite: true,
		  prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
		  nextArrow: "<i class='icon-chevron-right slick-next'></i>",
		  responsive: [{
			  breakpoint: 1199,
			  settings: {
				"slidesPerRow": 3,
				"slidesToScroll": 1 / 3,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				"slidesPerRow": 2,
				"slidesToScroll": 1 / 2,
			  }
			},
			{
			  breakpoint: 767,
			  settings: {
				"slidesPerRow": 1,
				"slidesToScroll": 1,
			  }
			},
		  ]
		});
  
	  } else {
  
		// Setup the slider
		$('.dn-image-hero-repeats').slick({
		  "slidesToShow": 4,
		  "slidesToScroll": 1,
		  infinite: true,
		  prevArrow: "<i class='icon-chevron-left slick-prev'></i>",
		  nextArrow: "<i class='icon-chevron-right slick-next'></i>",
		  responsive: [{
			  breakpoint: 1199,
			  settings: {
				"slidesToShow": 3,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				"slidesToShow": 2,
			  }
			},
			{
			  breakpoint: 767,
			  settings: {
				"slidesToShow": 1,
			  }
			},
		  ]
		});
  
	  }
	}
})
  
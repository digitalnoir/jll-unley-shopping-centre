// GMap Style
// Please refer to this site: https://snazzymaps.com/
var GMapStyle = [{
    "featureType": "administrative",
    "elementType": "all",
    "stylers": [{
        "saturation": "-100"
    }]
}, {
    "featureType": "administrative.province",
    "elementType": "all",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "landscape",
    "elementType": "all",
    "stylers": [{
        "saturation": -100
    }, {
        "lightness": 65
    }, {
        "visibility": "on"
    }]
}, {
    "featureType": "poi",
    "elementType": "all",
    "stylers": [{
        "saturation": -100
    }, {
        "lightness": "50"
    }, {
        "visibility": "off"
    }]
}, {
    "featureType": "road",
    "elementType": "all",
    "stylers": [{
        "saturation": "-100"
    }]
}, {
    "featureType": "road.highway",
    "elementType": "all",
    "stylers": [{
        "visibility": "simplified"
    }]
}, {
    "featureType": "road.arterial",
    "elementType": "all",
    "stylers": [{
        "lightness": "30"
    }]
}, {
    "featureType": "road.local",
    "elementType": "all",
    "stylers": [{
        "lightness": "40"
    }]
}, {
    "featureType": "transit",
    "elementType": "all",
    "stylers": [{
        "saturation": -100
    }, {
        "visibility": "simplified"
    }]
}, {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [{
        "hue": "#ffff00"
    }, {
        "lightness": -25
    }, {
        "saturation": -97
    }]
}, {
    "featureType": "water",
    "elementType": "labels",
    "stylers": [{
        "lightness": -25
    }, {
        "saturation": -100
    }]
}];

jQuery(function($){
    

	// Add Google Map
	$('.gmap').each(function () {
		initMap($(this).data("lat"), $(this).data("lng"), $(this).data("icon"), $(this) );
	});

	

	function initMap(lat, lng, icon_url, $container) {

		var viewCentreLat = lat;
		var viewCentreLng = lng;
		if ($(window).width() > 991) {
			// viewCentreLat -= 0.0035;
			// viewCentreLng += 0.015;
		}
		var mapClass = $container
		var map = new google.maps.Map( mapClass[0], {
			scrollwheel: false,
			zoom: 15,
			styles: GMapStyle,
			center: new google.maps.LatLng(viewCentreLat, viewCentreLng),
			mapTypeId: 'roadmap',
			disableDefaultUI: true
		});

		// Disable map drag on mobile
		// if ( $(window).width() < 992 ) {
		// map.setOptions({draggable: false});
		// }

		var IconSize = new google.maps.Size(55, 72);
		if ($(window).width() < 992) {
			IconSize = new google.maps.Size(39, 52);
		}

		var feature = {
			position: new google.maps.LatLng(lat, lng),
		};
		var icon = {
			url: icon_url,
			scaledSize: IconSize,
		};
		var marker = new google.maps.Marker({
			position: feature.position,
			icon: icon,
			scaledSize: new google.maps.Size(10, 10),
			map: map
		});

		// create info window
		var infowindow = new google.maps.InfoWindow({
			content: $container.siblings(".bubble-info").html(),
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function () {

			infowindow.open(map, marker);

		});
	}
})
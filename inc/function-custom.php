<?php

/*
    Usage:
    This file contain project speficic function, you can copy any function
    from function-collection.php file and put here or any function
*/

    /**
     * Register widget area.
     */
    add_action( 'widgets_init', 'digitalnoir_widgets_init' );
    function digitalnoir_widgets_init() {
      
      //register main sidebar
      // register_sidebar( array(
      //   'name'          => __( 'Sidebar', 'digitalnoir' ),
      //   'id'            => 'sidebar-main',
      //   'description'   => '',
      //   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      //   'after_widget'  => '</aside>',
      //   'before_title'  => '<h3 class="widget-title">',
      //   'after_title'   => '</h3>',
      // ) );
      
      
      // register footer widget
      register_sidebar( array(
        'name'          => __( 'Footer 1', 'digitalnoir' ),
        'id'            => 'footer-1',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title" style="display:none">',
        'after_title'   => '</h4>',
      ) );
      
      register_sidebar( array(
        'name'          => __( 'Footer 2', 'digitalnoir' ),
        'id'            => 'footer-2',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
      ) );
      
      register_sidebar( array(
        'name'          => __( 'Footer 3', 'digitalnoir' ),
        'id'            => 'footer-3',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
      ) );
      
      register_sidebar( array(
        'name'          => __( 'Footer 4', 'digitalnoir' ),
        'id'            => 'footer-4',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
      ) );
    }


function function_social_media() {

   $generate_data = array(
       array(
         'data-title'  => 'Facebook',
         'data-icon'   => file_get_contents(get_template_directory_uri().'/img/social/sm-fb.svg'),
         'data-link'   => get_field('facebook_link', 'options'),
       ),
       array(
         'data-title'  => 'Twitter',
         'data-icon'   => file_get_contents(get_template_directory_uri().'/img/social/sm-fb.svg'),
         'data-link'   => get_field('twitter_link', 'options'),
       ),
       array(
         'data-title'  => 'Instagram',
         'data-icon'   => file_get_contents(get_template_directory_uri().'/img/social/sm-instagram.svg'),
         'data-link'   => get_field('instagram_link', 'options'),
       ),
       array(
         'data-title'  => 'LinkedIn',
         'data-icon'   => file_get_contents(get_template_directory_uri().'/img/social/sm-instagram.svg'),
         'data-link'   => get_field('linkedin_link', 'options'),
       ),
       array(
         'data-title'  => 'Youtube',
         'data-icon'   => file_get_contents(get_template_directory_uri().'/img/social/sm-instagram.svg'),
         'data-link'   => get_field('youtube_link', 'options'),
       ),
   );
   
   $merge = '<div class="social-media-wrapper">';
       foreach( $generate_data as $item ):
            if($item['data-link'] != ""):
               $merge .= '<a href="'.$item['data-link'].'" title="'.$item['data-title'].'">'.$item['data-icon'].'</a>';
               $merge .= '&nbsp;&nbsp;';
            endif; 
       endforeach;
   $merge .= '</div>';

   return $merge;

}

# https://codex.wordpress.org/Shortcode_API
# [social-media]
add_shortcode( 'social-media', 'function_social_media' ); 

/*-------------------------------
# Remove the Text Editor on specific Page
# https://gist.github.com/ramseyp/4060095
# https://www.isitwp.com/disable-content-editor-for-specific-page-template/
# https://wordpress.stackexchange.com/questions/273367/remove-editor-from-homepage
--------------------------------*/

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
   if (isset($_GET['post'])) {
      $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

      // $page_general_condition = in_array($post_id, array(0));

      $page_template = get_post_meta($post_id, '_wp_page_template', true);
      // $page_template_condition = in_array($page_template, array('page-templates/page-each-service.php')) ;
      $page_template_condition = in_array($page_template, array('default')) ;

      $page_front_condition = in_array($post_id, array(get_option('page_on_front')));

      if( !$page_template_condition ||  $page_front_condition ){
         remove_post_type_support('page', 'editor');
      }
   }
}

/**
 * Register a custom post type called "store".
 *
 * @see get_post_type_labels() for label keys.
 */
add_action( 'init', 'wpdocs_codex_store_init' );
function wpdocs_codex_store_init() {
  $labels = array(
      'name'                  => _x( 'Stores', 'Post type general name', 'textdomain' ),
      'singular_name'         => _x( 'Store', 'Post type singular name', 'textdomain' ),
      'menu_name'             => _x( 'Stores', 'Admin Menu text', 'textdomain' ),
      'name_admin_bar'        => _x( 'Store', 'Add New on Toolbar', 'textdomain' ),
      'add_new'               => __( 'Add New', 'textdomain' ),
      'add_new_item'          => __( 'Add New Store', 'textdomain' ),
      'new_item'              => __( 'New Store', 'textdomain' ),
      'edit_item'             => __( 'Edit Store', 'textdomain' ),
      'view_item'             => __( 'View Store', 'textdomain' ),
      'all_items'             => __( 'All Stores', 'textdomain' ),
      'search_items'          => __( 'Search Stores', 'textdomain' ),
      'parent_item_colon'     => __( 'Parent Stores:', 'textdomain' ),
      'not_found'             => __( 'No stores found.', 'textdomain' ),
      'not_found_in_trash'    => __( 'No stores found in Trash.', 'textdomain' ),
      'featured_image'        => _x( 'Store Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'archives'              => _x( 'Store archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
      'insert_into_item'      => _x( 'Insert into store', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
      'uploaded_to_this_item' => _x( 'Uploaded to this store', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
      'filter_items_list'     => _x( 'Filter stores list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
      'items_list_navigation' => _x( 'Stores list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
      'items_list'            => _x( 'Stores list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
  );

  $args = array(
      'labels'             => $labels,
      'public'             => false,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'store' ),
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'menu_position'      => 5,
      'supports'           => array( 'title' ),
  );

  register_post_type( 'store', $args );

  // category
  $labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Categories', 'textdomain' ),
		'all_items'         => __( 'All Categories', 'textdomain' ),
		'parent_item'       => __( 'Parent Category', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Category:', 'textdomain' ),
		'edit_item'         => __( 'Edit Category', 'textdomain' ),
		'update_item'       => __( 'Update Category', 'textdomain' ),
		'add_new_item'      => __( 'Add New Category', 'textdomain' ),
		'new_item_name'     => __( 'New Category Name', 'textdomain' ),
		'menu_name'         => __( 'Category', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => false,
		'rewrite'           => array( 'slug' => 'store_cat' ),
	);

  register_taxonomy( 'store_cat', array( 'store' ), $args );
  
}


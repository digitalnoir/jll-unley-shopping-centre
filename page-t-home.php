<?php
/*
Template Name: Homepage
 */
get_header(); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php while ( have_posts() ) : the_post(); ?>
            <article>

                <?php # Template Part | Footer Map
                get_template_part('blocks/section/section_store_category'); ?>  

                <?php # Template Part | Featured Post
                get_template_part('blocks/section/section_featured_post'); ?>  

                <?php # Template Part | Newsletter
                get_template_part('blocks/section/section_newsletter'); ?>  

                <?php # Template Part | Instagram
                get_template_part('blocks/section/section_instagram'); ?>  

				
            </article>
            <?php dn_post_edit_link(); ?>
        <?php endwhile; // end of the loop. ?>
    </main>
 
</div>
<?php get_footer();
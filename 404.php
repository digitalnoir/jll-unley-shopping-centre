<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Digital_Noir_Starter_Pack
 */

get_header();

	dn_enqueue_style('404');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				
				<h1>Oops!</h1>
				<div class="sorry">The page you’re looking for can’t be found.</div>
				<div class="back-to-homepage"><a href="<?php echo HOME_URL ?>/" rel="follow">Back to home</a></div>
				<div class="the-404">4<span>0</span>4</div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

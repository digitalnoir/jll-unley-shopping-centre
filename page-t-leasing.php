<?php
/*
Template Name: Leasing Form
 */
get_header(); ?>

<div class="site-content">
	<div class="content-area">
		<main id="main" class="site-main" >
			<?php while ( have_posts() ) : the_post(); ?>
			<article>

			<?php # Template Part | Footer Map
                get_template_part('blocks/builder/contact'); ?>
				
			</article>
			<?php dn_post_edit_link(); ?>
			<?php endwhile; // end of the loop. ?>
		</main>
	</div>
</div>
<?php get_footer();
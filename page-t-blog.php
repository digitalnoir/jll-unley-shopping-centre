<?php
/*
Template Name: Page Blog
 */
get_header(); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php while ( have_posts() ) : the_post(); ?>
            <article>

                <?php # Template Part | Footer Map
                get_template_part('blocks/section/section_blog_listing'); ?>  

				
            </article>
            <?php dn_post_edit_link(); ?>
        <?php endwhile; // end of the loop. ?>
    </main>
 
</div>
<?php get_footer();
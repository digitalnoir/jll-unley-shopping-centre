<div class="dn-cols">
    <?php dn_enqueue_style('content-columns'); ?>
    <div class="container">
        <div class="row">
            <?php
                // Dyncamically calculate the width of the columns based on how much content there is
                $col_width = 12 / count( get_sub_field('column_blocks') );
                if( have_rows('column_blocks') ):
                    while ( have_rows('column_blocks') ) : the_row();
                    ?>
                        <div class="col-sm-<?php echo $col_width; ?>">
                            <?php the_sub_field( "wysiwyg_editor" ); ?>
                        </div>
                    <?php
                    endwhile;
                endif;
            ?>
        </div>
    </div>
</div>
<?php

    // Used for outputting images
    $output_images = array();
    $total_put_elements = 0;

?>
<div class="sliding-gallery">

    <?php
        dn_enqueue_style('slick');
        dn_enqueue_style('magnific');
        dn_enqueue_style('sliding-gallery');

        wp_enqueue_script('js-sliding-gallery');
    ?>


    <div class="dn-image-hero-repeats flex-container">
        
        <?php

            $gallery =  get_sub_field('image_blocks') ;
            if( sizeof($gallery) > 0 ):
                foreach($gallery as $attachment){
                $attachment_id = $attachment['ID'];    
                

                    $is_video = get_post_meta( $attachment_id, '_gallery_video', true );
                    $gallery_class = $is_video != '' ? 'video-link' : 'image-link';

                    $gallery_link = $is_video != '' ? $is_video : wp_get_attachment_image_src( $attachment_id , "dn-large" )[0];

                    ob_start();
                    echo '<a class="image-container special-link '. $gallery_class .'" href="' . $gallery_link . '" >';
                    echo '<div class="inner">';
                    echo dn_get_background_image( $attachment_id  );
                    echo '<div class="overlay"></div>';
                    echo '</div>';
                    echo '</a>';
                    //echo "\n\r";echo "\n\r";
                    $temp_image = ob_get_clean();

                    $output_images['c'.$total_put_elements] = $temp_image;
                    $total_put_elements++;
                    
                }
            endif;


            // If the number of images is not a multiple of 8, but greater than 8
            // Replay those images to fill in the blanks, starting from the beggining
     
            if ( $total_put_elements % 8 != 0 ) {
                for ( $i1 = 0;     $i1 < (8 - ($total_put_elements % 8))   ; $i1++ ) {
                    $key = 8 - ( $i1 % 8 );
                    $combine_key = sizeof( $output_images ) + $i1;
                    $output_images['c'.$combine_key] = $output_images[ 'c'.$i1 ];
                }
            }

            foreach($output_images as $image){
                echo $image;
            }

            
        ?>
    </div>
</div>
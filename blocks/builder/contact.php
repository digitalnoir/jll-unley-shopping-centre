<?php dn_enqueue_style('contact'); ?>

<div class="dn-contact">
    <div class="container-flow">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6 right">
                <div class="right-content">
                    <?php the_field( "form_header" ); ?>
                    <?php gravity_form( get_field("contact_form")['id'], false, false, false, null, false, 0, true ); ?>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-6 left pull-right">
                <div class="left-content">
                    <div class="text-content">
                        <?php the_field("left_content"); ?>
                    </div>
                </div>
            </div>
            

        </div>
    </div>
</div>
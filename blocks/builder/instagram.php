<?php dn_enqueue_style('instagram') ?>

<?php $instagram_username = get_sub_field("username"); ?>
<script>
    (function($){
        $(window).on('load', function(){
            var insta_name = "<?php echo $instagram_username; ?>";
            $.instagramFeed({
                'username': insta_name,
                'container': ".instagram-feed1",
                'display_profile': false,
                'display_biography': false,
                'display_gallery': true,
                'callback': null,
                'styling': true,
                'items': 4,
                'items_per_row': 4,
                'margin': 0
            });
        });
    })(jQuery);
</script>
<section class="section-instagram">
    <div class="section-shortcodes-container">
        <div class="instagram-wrapper">
            <div class="instagram-feed1"></div>
        </div>
    </div>
</section>
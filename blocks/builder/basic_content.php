<div class="basic-content has-padding">
    <?php dn_enqueue_style('basic-content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php the_sub_field("editor"); ?>
            </div>
        </div>
    </div>
</div>
<div class="dn-logo-gallery">

    <?php dn_enqueue_style('logos-gallery') ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <?php 
                    $title = get_sub_field('title');
                    if ( $title ) { ?>
                        <h2><?php echo $title; ?></h2>
                    <?php } ?>

                <?php 

                $images = get_sub_field('logos');
                $size = 'image-420'; // (thumbnail, medium, large, full or custom size)

                if( $images ): ?>
                    <ul>
                        <?php foreach( $images as $image ): 
                            
                            $lazy_img = array(
                                'class'=> 'lazyload',
                                'src' => 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
                            );

                            echo '<li>' . dn_get_attachment_image_lazy( $image['ID'], $size ) .'</li>';

                         endforeach; ?>
                    </ul>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
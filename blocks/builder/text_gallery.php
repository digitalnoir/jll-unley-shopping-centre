<?php
    $heading = get_sub_field('heading');
    $subheading = get_sub_field('subheading');
    $content = get_sub_field('content');
    $responsive_gallery = get_sub_field('responsive_layout');
    $gallery = get_sub_field('gallery');
?>
<div class="text-gallery">
    <?php 
        dn_enqueue_style('text-gallery');
        wp_enqueue_script('js-text-gallery');
    ?>
    <div class="content">
        <div class="inner">
            <?php echo $heading != '' ? '<h2 class="heading">'. $heading .'</h2>' : ''; ?>
            <?php echo $subheading != '' ? '<h3 class="h4 sub-heading">'. $subheading .'</h3>' : ''; ?>
            <?php echo $content; ?>
        </div>
    </div>
    <div class="gallery-slider">
        <?php if(!empty($gallery)) : ?>
        <div class="text-gallery-slider responsive-<?php echo $responsive_gallery ?>">
            <?php
                foreach($gallery as $image){
                    
                    $image_ID = $image['ID'];
                    echo '<div class="slide-item">'. dn_get_background_image( $image_ID  ) .'</div>';
                }
            ?>
        </div>
    <?php endif; ?>
    </div>
</div>
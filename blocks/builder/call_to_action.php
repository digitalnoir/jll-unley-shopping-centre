<div class="dn-cta">
    <?php dn_enqueue_style('call-to-action') ?>
    <?php the_field('cta_background') ?>
    <div class="container">

            <div class="cta-content">
                <div>
                    <h2><?php the_field("title"); ?></h2>
                    <?php echo wpautop( get_field("content") ); ?>
                </div>

                <?php render_link_field_helper('link', 'dn-button large'); ?>

            </div>

    </div>
</div>
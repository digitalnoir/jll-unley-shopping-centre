<?php

    /* 
        This file being use for:
        1. Image - Text Alternating Rows
        2. Image - Text Alternating Rows (Full)
    */


    // Used to keep track of the arrangement of the dividers
    global $HorizDivierState;

    dn_enqueue_style('content-horiz-divider');
?>

<div class="dn-horiz-divider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 <?php if ( $HorizDivierState % 2 ) { echo "col-sm-push-6"; } ?> image-container">
                <?php the_sub_field("image"); ?>
            </div>
            <div class="col-sm-6 <?php if ( $HorizDivierState % 2 ) { echo "col-sm-pull-6"; } ?> horiz-contents">
                <div class="dn-card-small">
                    <h3><?php the_sub_field("title"); ?></h3>
                    <?php echo wpautop( get_sub_field("text") ); ?>
                    <?php render_link_helper('link', 'dn-button feature-button'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="single-team col-md-4 col-sm-6 col-xs-12">
    
    <div class="special-link image-container sliding-block">
        <?php echo get_sub_field('image'); ?>
    </div>

    <div class="post-content">

        <?php $contact = get_sub_field('contact') ?>
        <h3><?php echo $contact['name'] ?></h3>
        <div class="position"><h4 class="h5"><?php echo $contact['position'] ?></h4></div>
        <?php echo wpautop( get_sub_field('description') ); ?>
        <?php render_link_helper('link', 'dn-button feature-button'); ?>

        <?php
            // not in use for most project
            // to make this happen please create the social_media custom field

            /*
            $is_social = get_sub_field('social_media');
            if(is_array($is_social) && sizeof($is_social) > 0 ){
                echo '<ul class="social-profile">';

                foreach( $is_social as $social){
                    if($social == 'social_email'){
                        echo '<li class="'. $social .'"><a href="mailto:'. get_sub_field($social) .'" target="_blank" rel="nofollow"><i class="icon icon-mail"></i></a></li>';
                    }else{
                        $the_icon = str_replace('social_','icon-', $social );
                        echo '<li class="'. $social .'"><a href="'. get_sub_field($social) .'" target="_blank" rel="nofollow"><i class="icon '.$the_icon.'"></i></a></li>';
                    }
                }

                echo '</ul>';
            }
            */
        ?>

        
    </div>
</div>
<?php
    wp_enqueue_script( 'js-matchHeight');
?>
<div class="dn-single-post dn-basic-post-card">

    <?php dn_enqueue_style('content-post-block') ?>

    <?php if ( has_post_thumbnail() ) { ?>
        <div class="special-link image-container"><a href="<?php the_permalink() ?>">
            <?php echo dn_get_background_image( get_post_thumbnail_id() ); ?>
            <?php
                $post_terms = wp_get_post_terms( get_the_ID(), 'category' );
                echo '<span class="cat">'. $post_terms[0]->name.'</span>';
            ?>
        </a></div>
    <?php } ?>
    <div class="post-content">
        <h3 class="loop-title"><a class="special-link" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>

        <a href="<?php the_permalink(); ?>" class="dn-button outline">Read More</a>
    </div>
</div>
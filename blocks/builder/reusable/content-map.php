<?php

    $google_map = get_field("footer_maps",'option');

?>
<div class="gmap" data-lat="<?php echo $google_map['lat']; ?>" data-lng="<?php echo $google_map['lng']; ?>" data-icon="<?php echo THEME_URL; ?>/img/marker.svg"></div>
<div id="bubble-info" class="bubble-info hidden">
    <?php the_field("footer_map_buble", "options"); ?>
</div>
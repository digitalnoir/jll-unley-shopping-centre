<?php 

    // disable double hero on archive page
    if( ! is_singular('page') ){
        return;
    }

    dn_enqueue_style('hero');
    $layout = get_field('hero_layout');
    $hero_banner_values =  get_field("hero_content", get_the_ID());
    $hero_banner_header = get_field('hero_heading') != '' ? get_field('hero_heading') : get_the_title();
    $hero_banner_content = get_field('hero_description');
    $hero_banner_image = get_field('hero_image');
?>

<?php if($layout == 'banner_large'): ?>
    <?php if( is_front_page() ) : ?>
<div class="hero-section " >
    <div class="mega-slick-slider-container">

                <div class="hero-section-wrapper ">
                       <div class="hero-section-left">
                           <div class="hero-content-parent">
                               <div class="hero-content-child">
                                   <?php if($hero_banner_header): ?>
                                      <h1 class="hero-title"><?php echo $hero_banner_header; ?></h1>
                                   <?php endif; ?>
                                   <?php
                                        if( trim($hero_banner_content) != '' ) :
                                            echo wpautop($hero_banner_content);
                                        endif;
                                    ?>
                               </div>
                           </div>
                       </div>

                       <div class="hero-section-right">
                           <?php echo $hero_banner_image; ?>
                       </div>

                </div>
    </div>
</div>
<?php else: ?>
        <div class="casual-hero">
            <?php echo $hero_banner_image ?>
            <div class="inner"><h1 class="hero-title"><?php echo $hero_banner_header; ?></h1></div>
        </div>
    <?php endif ?>
<?php else: ?>
    <div class="simple-header">
        <div class="container"><h1 class="hero-title"><?php echo $hero_banner_header ?></h1></div>
    </div>
<?php endif; ?> 
<div class="dn-hover-cards has-padding">
    <?php dn_enqueue_style('feature-tiles'); ?>

    <div class="container card-container">
        <div class="row">
        <?php
            if( have_rows('store_category_cards') ):
                while ( have_rows('store_category_cards') ) : the_row();
                ?>
                    <div class="col-lg-4 col-sm-4 col-xs-12 single-card-container">
                        <div class="dn-hover-card">

                        <?php
                            $link = get_sub_field('link');
                            echo '<a class="special-link" href="'. $link['url'] .'">';
                        ?>

                            <?php the_sub_field("image") ?>

                            <div class="content">
                                <div class="content-inner">
                                    <div class="dn-card-small">

                                        <h3 class="h2"><?php the_sub_field("title"); ?></h3>
                                        
                                    </div>
                                </div>
                            </div>
                            <?php
                                echo '</a>';
                            ?>
                        </div>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>

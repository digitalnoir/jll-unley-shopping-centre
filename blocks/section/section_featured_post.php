<div class="dn-news-basic dn-block-flex">
    
    <?php dn_enqueue_style('news-listing') ?>

    <?php if ( get_field("featured_post_title") ) { ?>
        <div class="container-fluid intro-content">
            <div class="row">
                <div class="col-xs-12"><h2><?php echo get_field("featured_post_title"); ?></h2></div>
            </div>
        </div>
    <?php } ?>

    <div class="container-fluid post-container">
        <div class="row">
        <?php

        $posts_per_page = get_field('featured_post_number_of_post') != '' ? get_field('featured_post_number_of_post') : 6;
        $column_per_row = get_field('featured_post_number_of_column_per_row') != '' ? get_field('featured_post_number_of_column_per_row') : 2;
        $category       = get_field('featured_post_post_category');

        if ($column_per_row == 1) {
           $column_class = 'px-0 col-sm-12';
        }elseif($column_per_row == 2){
           $column_class = 'px-0 col-sm-6';
        }elseif($column_per_row == 3){
           $column_class = 'px-0 col-sm-4';
        }elseif($column_per_row == 4){
           $column_class = 'px-0 col-sm-3';
        }else{
           $column_class = 'col-sm-6';
        }

        $the_query = new WP_Query( array(
            'post_type' => 'post',
            'posts_per_page' => 4,
            'ignore_sticky_posts' => true
        ));

        if ( $the_query->have_posts() ) {
            $i = 0;
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $i++;
                ?>
                <div class="px-0 col-md-3 col-sm-6 col-xs-12 feat-loop-<?php echo $i ?>"><?php get_template_part('blocks/builder/reusable/content', 'post-block'); ?></div>
                <?php
            }
            wp_reset_postdata();
        }
        ?>
        </div>
    </div>
</div>
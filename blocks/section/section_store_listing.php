<?php dn_enqueue_style('store-catergory-listing'); ?>
<div class="dn-hover-category-listing has-padding">

       <div class="container intro-content ">
           <div class="row">
               <div class="col-xs-12">
                    <div class="toggle-filter visible-xs"><a class="special-link" href="#">Filter stores <span><img src="<?php echo THEME_URL ?>/img/next.svg" alt=""/></span></a></div>
                   <div class="category-button-wrapper toggled">
                       <?php

                          $terms = get_terms('store_cat', array('hide_empty'=>false));                         
                          $all_array = array();
                          $found = false;

                          ob_start();
                          foreach($terms as $cat){
                            $all_array[] = $cat->slug;
                            $active = '';
                            if($_GET['cat'] == $cat->slug){
                              $found = true;
                              $active = 'active';
                            }
                            echo '<a href="javascript:void(0)" data-name=".'. $cat->slug .'" class="dn-button '. $active .' feature-button category-button filter--button">'. $cat->name .'</a>';
                          }
                          $all_filter = ob_get_clean();

                          $all_active = $found == false ? 'active' : '';

                          echo '<a href="javascript:void(0)" data-name="*" class="dn-button feature-button category-button filter--button '.$all_active.'">All Stores</a>';
                          echo $all_filter;

                       ?>
                   </div>
               </div>
               <div class="col-xs-12">
                   <?php
                    $args = array(
                      'post_type' => 'store',
                      'posts_per_page' => -1,
                      'post_status' => 'publish'
                    );

                    $the_query = new WP_Query( $args );
                    
                    $active_init = isset( $_GET['cat'] ) && in_array( $_GET['cat'], $all_array ) ? '.'.$_GET['cat'] : '*';
                    

                   ?>
                   <div class="store-category-wrapper-parent" data-active="<?php echo $active_init ?>">

                   <?php

                    if ( $the_query->have_posts() ) {
                      while ( $the_query->have_posts() ) {
                          $the_query->the_post();
                          $terms = wp_get_post_terms( get_the_ID(), 'store_cat');
                          $all = array();
                          foreach($terms as $term){
                            $all[] = $term->slug;
                          }
                          ?>
                            <div class="element-item <?php echo implode(' ', $all) ?>">
                              <div href="javascript:void(0)" class="special-link">
                                  
                                <div class="inner">
                                  <div class="images"><div class="pict"><?php echo dn_get_background_image( get_field('store_logo') ) ?></div></div>
                                  <div class="store-number">Shop Number <?php the_field('store_number') ?></div>
                                  <div class="details">
                                    <div>
                                    <div class="title-number">
                                      <h3><?php the_title() ?></h3>
                                      <strong>Shop Number <?php the_field('store_number') ?></strong>
                                    </div>
                                    <?php 
                                      if( trim( get_field('phone_number') != '' ) ){
                                        echo '<div class="phone">Ph. '. get_field('phone_number') .'</div>';
                                      }
                                      if( trim( get_field('website_link') != '' ) ){
                                        echo '<div class="web"><a href="'. get_field('website_link') .'" target="_blank">Visit website</a></div>';
                                      }
                                    ?>
                                    </div>
                                  </div>
                                </div>

                                </div>
                            </div>
                          <?php
                      }
                    } 
                    /* Restore original Post Data */
                    wp_reset_postdata();

                  ?>

                   </div>

               </div>
           </div>
       </div>


  <div id="store-map">
      <div class="container">
          <div class="map-holder">
                <h2><?php the_field('full_map_title') ?></h2>
                <div class="image-holder"><?php echo dn_get_attachment_image_lazy( get_field('store_image'), 'full' ) ?></div>
          </div>
      </div>
  </div>

</div>
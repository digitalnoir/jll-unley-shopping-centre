<div class="map-contact business-hour">
   <?php 
       dn_enqueue_style('maps-contact');
       wp_enqueue_script('js-maps');
   ?>
   <div class="contact">
       <div class="inner">
           <div class="address"><?php the_field('footer_contact_details','option') ?></div>
           <div class="business">
               <?php
                   $hours = get_field('footer_business_hour','option');
                   if(!empty($hours)){
                       echo '<ul>';
                       foreach($hours as $day){
                           echo '<li><strong>'. $day['day'] .'</strong> <div class="hour">'. $day['time'] .'</div></li>';
                       }
                       echo '</ul>';
                   }
               ?>
           </div>
       </div>
   </div>
   <div class="map">
       <?php get_template_part('blocks/builder/reusable/content', 'map'); ?>
   </div>
</div>

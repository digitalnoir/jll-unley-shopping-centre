<?php $acf_newsletter_title = get_field('newsletter_title', get_the_ID());; ?>
<?php $acf_newsletter_sub_title = get_field('newsletter_sub_title', get_the_ID());; ?>
<?php $acf_newsletter_form = get_field('newsletter_form', get_the_ID());; ?>
<?php dn_enqueue_style('basic-content'); ?>
<div class="basic-content has-padding home-newsletter">
    <?php the_field('newsletter_bg') ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            	 <h2><?php echo $acf_newsletter_title; ?></h2>
            	 <p><?php echo $acf_newsletter_sub_title; ?></p>
                <?php gravity_form($acf_newsletter_form['id'], false, false, false, null, true, 0, true ); ?>
            </div>
        </div>
    </div>
</div>


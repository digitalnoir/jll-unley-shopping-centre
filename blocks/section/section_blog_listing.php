<div class="dn-news-basic dn-block-flex">
    
    <?php dn_enqueue_style('blog-category-listing') ?>


    <div class="container intro-content">
        <div class="row">
            <div class="category-button-wrapper">
               
               <?php
                  $terms = get_terms('category', array('hide_empty'=>false));
                  echo '<a class="dn-button feature-button" href="'. get_term_link( $term->term_id ) .'">'. $term->name .'</a>';
                  foreach($terms as $term){
                     echo '<a class="dn-button feature-button" href="'. get_term_link( $term->term_id ) .'">'. $term->name .'</a>';
                  }
                ?>
                  
                <a href="<?php echo get_permalink() . "?filter=category-3"; ?>"  class="dn-button feature-button 
                  <?php echo (( $_GET["filter"] == 'category-3' ) ? 'active' : ''); ?>  ">Category 3</a>
            </div>
        </div>
    </div>


    
        <?php

        $posts_per_page = get_sub_field('number_of_post') != '' ? get_sub_field('number_of_post') : 6;
        $column_per_row = get_sub_field('number_of_column_per_row') != '' ? get_sub_field('number_of_column_per_row') : 2;
        $category       = get_sub_field('post_category');

        if ($column_per_row == 1) {
           $column_class = 'col-sm-12';
        }elseif($column_per_row == 2){
           $column_class = 'col-sm-6';
        }elseif($column_per_row == 3){
           $column_class = 'col-sm-4';
        }elseif($column_per_row == 4){
           $column_class = 'col-sm-3';
        }else{
           $column_class = 'col-sm-4';
        }

        $column_class = 'col-sm-4';

        $all_terms    =  get_terms(array('taxonomy' => 'category','hide_empty' => false,'fields' => 'id=>slug'));

        ?>

            <?php
            # For Pagination (Optional)
            # Set the "paged" parameter (use 'page' if the query is on a static front page)
            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
            
            # Parameter
            $site_blog_args = array (
              'post_type' => array( 'post', ),
              'posts_per_page'  => 9,  # -1 for all
              'order'   => 'DESC',  # Newest
              'orderby' => 'date',  # 'rand' 'post__in'
              'paged'  => $paged,  # For Pagination
              'tax_query' => array(
                 'relation' => 'OR', # ('AND','OR')

                 array(
                    'field'    => 'slug',           #  ('term_id', 'name', 'slug', 'term_taxonomy_id')
                    'taxonomy' => 'category',     #  Taxonomy Name
                    'operator' => 'IN',                #  ('IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS')
                    'terms'    =>  (( isset($_GET["filter"]) && $_GET["filter"] != 'all' ) ? array($_GET["filter"]) : $all_terms),        #  (int, string, array)

                    # Select all Terms
                    # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'names'));
                    # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'id=>slug'));
                    # 'terms'    =>  get_terms(array('taxonomy' => 'taxonomy-name','hide_empty' => false,'fields' => 'tt_ids'));
                 ),
              ),
              #'category__not_in' => array(2),

            );
            
            # Connect Loop to Parameter
            $site_blog_query = new WP_Query( $site_blog_args );
            
            # For Pagination Issue (Optional)
            $temp_query = $wp_query;
            $wp_query   = NULL;
            $wp_query   = $site_blog_query;
            ?>
            
            <?php
            # Loop
            if ( $site_blog_query->have_posts() ) : ?>
              <div class="container post-container">
                  <div class="row">
                <?php while ( $site_blog_query->have_posts() ) : $site_blog_query->the_post(); ?>
                  <div class=" <?php echo $column_class; ?>"><?php get_template_part('blocks/builder/reusable/content', 'post-block'); ?></div>
                <?php endwhile; ?>
                  </div>
                  <?php if(get_the_posts_pagination()): ?>
                     <!-- pagination -->
                        <div class="row">
                             <div  id="container-pagination" class="text-center">
                                    <div class="col-sm-12">
                                          <?php echo the_posts_pagination(array(
                                              'mid_size'  => 3,
                                              'prev_text' => __( '<< Newer', 'textdomain' ),
                                              'next_text' => __( 'Older >>', 'textdomain' ),
                                          ) ); ?>   
                                    </div>
                             </div>
                        </div>
                  <?php endif; ?>
              </div>
               <!-- Parent Wrapper Here-->
            
              <?php wp_reset_query(); ?>
            <?php else : ?>
               <?php # Template Part | Blog
               get_template_part('template-parts/general/content-no-post'); ?>
            <?php endif; ?>

      
</div>
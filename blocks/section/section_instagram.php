<?php dn_enqueue_style('instagram') ?>

<?php $instagram_username = get_field("instagram_username"); ?>
<?php $instagram_instagram_title = get_field("instagram_title"); ?>
<?php $instagram_instagram_sub_title = get_field("instagram_sub_title"); ?>
<script>
    (function($){
        $(window).on('load', function(){
            var insta_name = "<?php echo $instagram_username; ?>";
            $.instagramFeed({
                'username': insta_name,
                'container': ".instagram-feed1",
                'display_profile': false,
                'display_biography': false,
                'display_gallery': true,
                'callback': null,
                'styling': true,
                'items': 4,
                'items_per_row': 4,
                'margin': 0
            });
        });
    })(jQuery);
</script>
<section class="section-instagram">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-12">
                 <h2><?php echo $instagram_instagram_title; ?></h2>
                 <p><a href="<?php echo get_field('instagram_link') ?>" target="_blank" class="special-link"><?php echo $instagram_instagram_sub_title; ?></a></p>
                 <div class="section-shortcodes-container">
                     <div class="instagram-wrapper">
                         <div class="instagram-feed1"></div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</section>
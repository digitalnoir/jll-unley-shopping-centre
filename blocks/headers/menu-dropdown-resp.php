<div class="mobile-menu-wrapper">
    <div class="mobile-menu-holder">
        
        <?php wp_nav_menu( array( 'theme_location' => 'header', 'depth' => 2 ) ); ?>

    </div><!-- .mobile-menu-holder -->
</div>
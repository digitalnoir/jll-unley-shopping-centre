<div class="shader-search"></div>
<div class="header-search-form fullscreen" style="display:none">
    <?php dn_enqueue_style('search-form-fullscreen') ?>

    <div class="form-container">
        <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
            <label for="search-full-input">Search for</label>
            <input id="search-full-input" type="text" class="search-field" placeholder="Enter keyword..." value="<?php echo get_search_query() ?>" name="s" />
            <button type="submit" class="search-submit"><i class="icon-search"></i></button>
        </form>
    </div>
    <div class="fullscreen-shader"></div>
</div>
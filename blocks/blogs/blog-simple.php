<?php
/* 
    Simple Blog
    only show image on left and description on the right
*/
get_header();

// Enqueue the required style & script
dn_enqueue_style('blog-simple');
wp_enqueue_script('js-infinite-scroll');
wp_enqueue_script( 'js-matchHeight');


// Check whether the page has next page
ob_start();
next_posts_link();
$next_posts_link = ob_get_clean();

// Only print if has next link
$data_infinite = $infinite_status;
if($next_posts_link != ''){
	$data_infinite = 'data-infinite-scroll=\'{
		"path": ".next_posts_link a",
		"append": ".dn-single-post",
		"history": false,
		"status": ".page-load-status"
	}\'';

	$infinite_status = '
				<div class="page-load-status" style="display:none">
					<p class="infinite-scroll-request">Loading...</p>
					<p class="infinite-scroll-last"></p>
					<p class="infinite-scroll-error">No more pages to load</p>
				</div>
	';
}

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php get_template_part('blocks/blogs/part-header') ?>

			<section class="results">
				<div class="container">

				<div class="toggle-filter visible-xs"><a class="special-link" href="#">Filter news <span><img src="<?php echo THEME_URL ?>/img/next.svg" alt=""/></span></a></div>
                   
					<div class="category-button-wrapper toggled">
					
					<?php
						$category = get_queried_object();
						$is_parent_news = get_option( 'page_for_posts' ) == $category->ID ? 'active' : '';
						$terms = get_terms('category', array('hide_empty'=>false));
						echo '<a class="dn-button feature-button filter--button '. $is_parent_news .'" href="'. get_permalink( get_option( 'page_for_posts' ) ) .'">All</a>';
						
						foreach($terms as $term){
							$is_active = $category->term_id == $term->term_id ? 'active' : '';
							echo '<a class="dn-button feature-button filter--button '. $is_active .'" href="'. get_term_link( $term->term_id ) .'">'. $term->name .'</a>';
						}
						?>
					</div>
			
    

					<div class="row">
						<div class="clearfix infinite-container" <?php echo $data_infinite ?>>
							<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>

								<?php 
									//get_template_part('blocks/blogs/loop-1-column');
									get_template_part('blocks/blogs/loop-3-column');
								?>

							<?php endwhile; ?>
							
							<?php else : ?>
								<h2 class="nofound">No post found!</h2>
							<?php endif; ?>
						</div>
					</div>
				</div>
				
				<?php // infinite loading bar ?>
				<div class="infinite-status-container container">
					<?php echo $infinite_status ?>
				</div>

				<?php // this is for infinite scroll ?>
				<div class="next_posts_link" style="display:none"><?php next_posts_link(); ?></div>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php 

    dn_enqueue_style('hero');
    $posts_page_id = get_option( 'page_for_posts' );
    $hero_banner_values =  get_field("hero_content", $posts_page_id );

    $layout = get_field('hero_layout', $posts_page_id);
    $hero_banner_values =  get_field("hero_content", $posts_page_id);
    $hero_banner_header = get_field('hero_heading', $posts_page_id) != '' ? get_field('hero_heading', $posts_page_id) : get_the_title();
    $hero_banner_content = get_field('hero_description', $posts_page_id);
    $hero_banner_image = get_field('hero_image', $posts_page_id);
?>

<?php if($layout == 'banner_large'): ?>
    <?php if( is_front_page() ) : ?>
        <div class="hero-section " >
            <div class="mega-slick-slider-container">

                        <div class="hero-section-wrapper ">
                            <div class="hero-section-left">
                                <div class="hero-content-parent">
                                    <div class="hero-content-child">
                                        <?php if($hero_banner_header): ?>
                                            <h1 class="hero-title"><?php echo $hero_banner_header; ?></h1>
                                        <?php endif; ?>
                                        <?php
                                                if( trim($hero_banner_content) != '' ) :
                                                    echo wpautop($hero_banner_content);
                                                endif;
                                            ?>
                                    </div>
                                </div>
                            </div>

                            <div class="hero-section-right">
                                <?php echo $hero_banner_image; ?>
                            </div>

                        </div>
            </div>
        </div>
    <?php else: ?>
        <div class="casual-hero">
            <?php echo $hero_banner_image ?>
            <div class="inner"><h1 class="hero-title"><?php echo $hero_banner_header; ?></h1></div>
        </div>
    <?php endif ?>
<?php else: ?>
    <div class="container"><h1 class="hero-title"><?php echo $hero_banner_header ?></h1></div>
<?php endif; ?> 
<div class="sidebar">
    <section class="sidebar-search">
    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
        <input type="text" class="search-field" placeholder="<?php echo esc_attr_x( 'Search here&hellip;', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" />
        <?php /* <button type="submit" class="search-submit"><i class='icon-search'></i> <?php echo esc_attr_x( 'Search', 'submit button' ) ?></button> /*/ ?>
    </form>
    </section>

    <section class="sidebar-category">
        <h3><?php echo __( 'Categories' ) ?></h3>
        <ul>
        <?php
            $args = array(
                'depth' => 1,
                'title_li' => ''
            );
            wp_list_categories($args);
        ?>
        </ul>
    </section>

    <section class="sidebar-archive">
        <h3><?php echo __( 'Archives' ) ?></h3>
        <ul>
        <?php
        $args = array(
                'type'            => 'monthly',
                'limit'           => '',
                'format'          => 'html', 
                'before'          => '',
                'after'           => '',
                'show_post_count' => false,
                'echo'            => 1,
                'order'           => 'DESC',
                'post_type'     => 'post'
            );
            wp_get_archives( $args );
        ?>
        </ul>
    </section>
</div>
<?php

/* Digital Noir Constant */
define('THEME_VERSION', '1.3');
define('THEME_URL', get_template_directory_uri());
define('THEME_DIR', get_template_directory());
define('HOME_URL', home_url() );
define('GOOGLE_MAP_KEY', 'AIzaSyDZLFJBMypZJEanN5GxXSwsxQzzR1nFi8o'); // live
//define('GOOGLE_MAP_KEY', 'AIzaSyAgrVVX2jVeSFXkgWubu9sgguu1cGYomwk'); // staging


/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'digital_noir_starter_pack_scripts' );
function digital_noir_starter_pack_scripts() {

	// Load our style
	//wp_enqueue_style( 'google-font', '//fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;1,300;1,400&family=Poiret+One&display=swap' );
	wp_enqueue_style( 'style-main', THEME_URL.'/style.css','', THEME_VERSION );	

	// Localize the script with new data
	$dn_variable = array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	);
	wp_localize_script( 'js-main', 'dn_variable', $dn_variable );

	// Enqueued script with localized data.
	wp_enqueue_script( 'js-main' );

	// disable guttenberg css
	wp_dequeue_style( 'wp-block-library' );

	/* if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	} */

	// PREPARE THE JS AND CSS FILE TO USE LATER
	wp_register_script('js-lazysize', THEME_URL. '/assets/dist/js/lazysizes.js', array('jquery'), THEME_VERSION, true); // put
	wp_register_script('js-isotope', THEME_URL. '/assets/dist/js/isotope.js', array('jquery'), THEME_VERSION, false);
	wp_register_script('js-magnific', THEME_URL. '/assets/dist/js/magnific.popup.js', array('jquery'), THEME_VERSION, true);
	wp_register_script('js-slick', THEME_URL. '/assets/dist/js/slick.js', array('jquery'), THEME_VERSION, true);
	wp_register_script('js-infinite-scroll', THEME_URL. '/assets/dist/js/infinite-scroll.js', array('jquery'), THEME_VERSION, true);
	wp_register_script('js-google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=' . GOOGLE_MAP_KEY, '', '', true );
	wp_register_script('js-instagram-feed', THEME_URL. '/assets/dist/js/jquery.instagramFeed.min.js', array('jquery'), THEME_VERSION, false);
	wp_register_script('js-matchHeight', THEME_URL. '/assets/dist/js/matchHeight.js', array('jquery'), THEME_VERSION, true);   

	// Blocks JS
	wp_register_script('js-sliding-gallery', THEME_URL. '/assets/dist/js/sliding-gallery.js', array('jquery', 'js-magnific', 'js-slick'), THEME_VERSION, true);
	wp_register_script('js-testimonial-slider', THEME_URL. '/assets/dist/js/testimonial-slider.js', array('jquery', 'js-slick'), THEME_VERSION, true);
	wp_register_script('js-text-gallery', THEME_URL. '/assets/dist/js/text-gallery.js', array('jquery', 'js-slick'), THEME_VERSION, true);
	wp_register_script('js-maps', THEME_URL. '/assets/dist/js/maps.js', array('jquery', 'js-google-maps'), THEME_VERSION, true);

	// all page should has lazypage
	wp_enqueue_script('js-slick');
	wp_enqueue_script('js-lazysize');
	wp_enqueue_script('js-instagram-feed');
	wp_enqueue_script('js-isotope');

	// Load our js
	wp_enqueue_script( 'js-main', THEME_URL . '/assets/dist/js/main.js', array('jquery'), THEME_VERSION, false );

}

/////////////////////
///// ACF Global ////
/////////////////////

// Include field type for ACF5
// $version = 5 and can be ignored until ACF6 exists
add_action('acf/include_field_types', 'dn_include_field_types_focal_point');
function dn_include_field_types_focal_point( $version ) {
	include_once( THEME_DIR . '/inc/acf-focal_point.php' );
}

// Add ACF Gravity form integration
require THEME_DIR . '/inc/acf-gravity_forms/acf-gravity_forms.php';



// INCLUDE NECESSARY FUNCTION 
require THEME_DIR . '/inc/function-white-label.php';
require THEME_DIR . '/emails/function-email.php';

// ONLY INCLUDE BASED ON WHERE THE CODE TRIGGERED 
if( !is_admin() ){
	require THEME_DIR . '/inc/function-front-end.php';
}else{
	require THEME_DIR . '/inc/function-back-end.php';
}

require THEME_DIR . '/inc/function-custom.php';
require THEME_DIR . '/inc/function-woocommerce.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

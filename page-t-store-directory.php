<?php
/*
Template Name: Store Directory
 */
get_header(); ?>

<div class="site-content">
    
    <main id="main" class="site-main" >
        <?php while ( have_posts() ) : the_post(); ?>
            <article>

                <?php # Template Part | Footer Map
                get_template_part('blocks/section/section_store_listing'); ?>  
				
            </article>
            <?php dn_post_edit_link(); ?>
        <?php endwhile; // end of the loop. ?>
    </main>

    <div class="visible-xs visible-sm store-link-mobile">
        <a href="#store-map" class="special-link jump-scroll">View the full map</a>
    </div>
 
</div>
<?php get_footer();
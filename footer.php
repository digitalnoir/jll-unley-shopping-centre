<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Digital_Noir_Starter_Pack
 */

?>

	</div><!-- #content -->

	<?php # Template Part | Footer Map
	get_template_part('blocks/section/footer_map'); ?>	

	<footer id="colophon" class="site-footer">
	    <div class="cols">
	        <div class="container-fluid">
	            <div class="row">
	            	 
                	<div class="footer-section">
						<a href="<?php echo get_home_url(); ?>" class="special-link"><img src="<?php echo THEME_URL; ?>/img/unley-logo.svg" alt="Unley Logo"></a>
	            	</div>
	           
 	      
					<div class="footer-section">
 	            	   	<?php dynamic_sidebar('footer-2'); ?>
 	            	</div>
 	            	 
                 	<div class="footer-section">
						<aside class="widget footer-widget widget_nav_menu">
							<h4 class="widget-title">Follow us</h4>
							<div class="menu-customer-service-container">
								<?php echo dn_print_social_icon(); ?>
							</div>
						</aside>		
 	            	</div>

                 	<div class="footer-section newsletter">
 	            		<?php dynamic_sidebar('footer-4'); ?>
					 </div>
					 
					<div class="dn-copy">
						<a href="https://www.digital-noir.com" target="_blank" rel="nofollow" class="special-link"><?php echo dn_get_svg('dn.svg') ?></a>
					</div>
	
					  
	            </div>
	        </div>
	    </div>

	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>